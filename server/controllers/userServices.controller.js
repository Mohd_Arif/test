const logger = require('../../config/logger');
const userService = require('../service/userServices');



const userInput1 = (req,res,next)=>{
  logger.trace("inside user input1 controller");
  let obj = req.params.id;
  if(!obj){
    return res.status(404).json({"success":false,"message":"pass object in params"});
  }
  userService.userInput1(obj)
  .then(data=>{
    logger.debug("resp",data);
    res.json({"success":true, "data":data});
  }).catch(err=>{
    return res.status(err.code?err.code:404).json({"success":false,"message":err});
  });
}

const userInput2 = (req,res,next)=>{
  logger.trace("inside user input2 controller");
  let obj = req.params.id;
  if(!obj){
    return res.status(404).json({"success":false,"message":"pass object in params"});
  }
  userService.userInput2(obj)
  .then(data=>{
    logger.debug("resp",data);
    res.json({"success":true, "data":data});
  }).catch(err=>{
    return res.status(err.code?err.code:404).json({"success":false,"message":err});
  });
}

module.exports = {
  userInput1,
  userInput2
};