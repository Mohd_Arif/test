const logger = require('../../config/logger');
const fs = require('fs');


let INPUT1_ELEMENTS = [];
let INPUT2_ELEMENTS = [];
let result1 = [];
let result2 = [];
let lastElementSendByInput2,lastElementSendByInput1;
let insertFlag1 = true;
let insertFlag2 = false;

const userInput1 = (userInput1)=>{
  logger.trace("inside user input 1",{userInput1});
  return new Promise(async (resolve,reject)=>{
    try{
      lastElementSendByInput1 = userInput1;
      INPUT1_ELEMENTS.push(lastElementSendByInput1);
      if(insertFlag1){
        result1.push(lastElementSendByInput1);
      }
      insertFlag1 = false;
      insertFlag2 = true;
      fs.writeFileSync(__dirname + "/../outputs/output1.txt", result1.join("\n"));



      if(INPUT2_ELEMENTS.length){
        result2=[];
        for(let element in INPUT2_ELEMENTS){
          result2.push(INPUT2_ELEMENTS[element]);
          if(INPUT1_ELEMENTS[element] != undefined){
            result2.push(INPUT1_ELEMENTS[element]);
          }
          else{
            break;
          }
        }
        fs.writeFileSync(__dirname + "/../outputs/output2.txt", result2.join("\n"));
      }


      resolve(userInput1);
    }
    catch(err){
      logger.fatal(err);
    }
  });
};

const userInput2 = (userInput2)=>{
  logger.trace("inside user input 2",{userInput2});
  return new Promise(async (resolve,reject)=>{
    try{
      lastElementSendByInput2 = userInput2;
      INPUT2_ELEMENTS.push(lastElementSendByInput2);
      if(insertFlag2){
        if(insertFlag1){
          result1[result1.length - 1] = lastElementSendByInput2;
        }
        else{
          result1.push(lastElementSendByInput2);
        }
        insertFlag1 = true;
        fs.writeFileSync(__dirname + "/../outputs/output1.txt", result1.join("\n"));
      }


      if(INPUT1_ELEMENTS.length){
        result2=[];
        for(let element in INPUT2_ELEMENTS){
          result2.push(INPUT2_ELEMENTS[element]);
          if(INPUT1_ELEMENTS[element] != undefined){
            result2.push(INPUT1_ELEMENTS[element]);
          }
          else{
            break;
          }
        }
        fs.writeFileSync(__dirname + "/../outputs/output2.txt", result2.join("\n"));
      }
      else{
        fs.writeFileSync(__dirname + "/../outputs/output2.txt", INPUT2_ELEMENTS[0]);
      }
      resolve(userInput2);  
    }
    catch(err){
      logger.fatal(err);
    }
  });
};


module.exports = {
  userInput1,
  userInput2
};
