const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/userServices.controller');
const router = express.Router();


router.route('/input1/:id').get(controller.userInput1);
router.route('/input2/:id').get(controller.userInput2);




module.exports = router;
